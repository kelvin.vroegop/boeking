package nl.first8.library.controller;

import nl.first8.library.domain.Member;
import nl.first8.library.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class MemberController {

    @Autowired
    private MemberRepository memberRepository;


    @GetMapping("/members")
    public List<Member> getAll() {
        return memberRepository.findAll();
    }

    @GetMapping("/members/{id}")
    public ResponseEntity<Member> getById( @PathVariable(value = "id") Long id) {
        Member member = memberRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Member not found on :: " + id));
        return ResponseEntity.ok(member);
    }

    @PostMapping("/members")
    public Member add(@RequestBody Member member) {
        return memberRepository.save(member);
    }

    @PutMapping("/memers/{id}")
    public ResponseEntity<Member> update(@PathVariable(value = "id") Long id, @RequestBody Member member) {
        Member memberFromDatabase = memberRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Member not found on :: " + id));

        memberFromDatabase.setName(member.getName());
        memberFromDatabase.setDateOfBirth(member.getDateOfBirth());

        Member updatedMember = memberRepository.save(memberFromDatabase);
        return ResponseEntity.ok(updatedMember);
    }

    @DeleteMapping("/members/{id}")
    public Map<String, Boolean> delete( @PathVariable(value = "id") Long id) {
        memberRepository.deleteById(id);
        return Map.of("deleted", true);
    }
}
