package nl.first8.library.controller;

import nl.first8.library.domain.Bluray;
import nl.first8.library.repository.BlurayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class BlurayController {

    @Autowired
    private BlurayRepository blurayRepository;

    @GetMapping("/blurays")
    public List<Bluray> getAll() {
        return blurayRepository.findAll();
    }

    @GetMapping("/blurays/{id}")
    public ResponseEntity<Bluray> getById(@PathVariable(value = "id") Long id) {
        Bluray bluray = blurayRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Bluray not found on :: " + id));
        return ResponseEntity.ok(bluray);
    }

    @PostMapping("/blurays")
    public Bluray add(@RequestBody Bluray bluray) {
        return blurayRepository.save(bluray);
    }

    @PutMapping("/blurays/{id}")
    public ResponseEntity<Bluray> update(@PathVariable(value = "id") Long id, @RequestBody Bluray bluray) {
        Bluray blurayFromDatabase = blurayRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Bluray not found on :: " + id));

        blurayFromDatabase.setTitle(bluray.getTitle());

        Bluray updatedBluray = blurayRepository.save(blurayFromDatabase);
        return ResponseEntity.ok(updatedBluray);
    }

    @DeleteMapping("/blurays/{id}")
    public Map<String, Boolean> delete(@PathVariable(value = "id") Long id) {
        blurayRepository.deleteById(id);
        return Map.of("deleted", true);
    }

    @PutMapping("/blurays/{id}/borrow")
    public ResponseEntity<Bluray> borrow(@PathVariable(value = "id") Long id) {
        Bluray bookFromDatabase = blurayRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Bluray not found on :: " + id));

        bookFromDatabase.setBorrowed(true);

        Bluray updatedBluray = blurayRepository.save(bookFromDatabase);
        return ResponseEntity.ok(updatedBluray);
    }

    @PutMapping("/blurays/{id}/handin")
    public ResponseEntity<Bluray> handin(@PathVariable(value = "id") Long id) {
        Bluray blurayFromDatabase = blurayRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Bluray not found on :: " + id));

        blurayFromDatabase.setBorrowed(false);

        Bluray updatedBluray = blurayRepository.save(blurayFromDatabase);
        return ResponseEntity.ok(updatedBluray);
    }
}
